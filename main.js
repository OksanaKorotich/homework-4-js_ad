'use strict';

let films = document.querySelector('.films');

function loadFilms(){
  const URL = 'https://ajax.test-danit.com/api/swapi/films';

  fetch(URL)
  .then(response => response.json())
  .then(data => {
    data.forEach(item => {
      const li = document.createElement('li');
      const summary = document.createElement('p');
      const personList = document.createElement("span");

      li.classList.add('title');
      summary.classList.add('text');
      personList.classList.add('persons')

      li.textContent = `Епізод: ${item.episodeId} Назва: ${item.name}`;
      summary.textContent = `Summary: ${item.openingCrawl}`;
      personList.textContent = 'Основні персонажі: ';

      films.append(li);
      li.append(summary);
      summary.after(personList)

      item.characters.forEach(character => {
        fetch(character)
        .then(response => response.json())
        .then(data => {

          let person = document.createElement("li");
          person.textContent = data.name;
          person.classList.add('person-name')
          personList.after(person)
        })
      })
    })
  })
}

loadFilms();









